import { Component, OnInit } from '@angular/core';
import { Passenger } from 'src/app/models/passenger.model';
import { StoreService } from 'src/app/services/store/store.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  passengers: Passenger[] = [];

  constructor(private storeService: StoreService) {}

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passenger) => {
      this.passengers = passenger;
    });
  }
}
