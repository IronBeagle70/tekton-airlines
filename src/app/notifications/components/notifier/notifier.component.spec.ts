import { DebugElement } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  flushMicrotasks,
  TestBed,
} from '@angular/core/testing';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';
import { NotificationsModule } from '../../notifications.module';
import { NotificationData, NotifierComponent } from './notifier.component';

describe('NotifierComponent', () => {
  let component: NotifierComponent;
  let fixture: ComponentFixture<NotifierComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NotificationsModule],
      declarations: [NotifierComponent],
      providers: [
        {
          provide: MAT_SNACK_BAR_DATA,
          useValue: {},
        },
        {
          provide: MatSnackBarRef,
          useValue: {
            dismiss: jasmine.createSpy('dismiss'),
          },
        },
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(NotifierComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('notifier component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display the message and button text', fakeAsync(() => {
    const notificationData: NotificationData = {
      message: 'Test message',
      buttonText: 'Test button text',
    };

    component.data = notificationData;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const messageEl = el.nativeElement.querySelector('.notifier__message');
      const buttonEl = el.nativeElement.querySelector('.notifier__btn button');

      expect(messageEl.textContent.trim()).toEqual('Test message');
      expect(buttonEl.textContent.trim()).toEqual('Test button text');
    });
  }));

  it('should display the correct message and button text', fakeAsync(() => {
    const notificationData: NotificationData = {
      message: 'Test message',
      buttonText: 'Test button text',
    };

    component.data = notificationData;
    fixture.detectChanges();

    const buttonElement = fixture.nativeElement.querySelector(
      '.notifier__btn button'
    );

    buttonElement.click();

    expect(component.snackBarRef.dismiss).toHaveBeenCalled();

    flushMicrotasks();
  }));
});
