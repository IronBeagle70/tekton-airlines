import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifierComponent } from './components/notifier/notifier.component';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";


@NgModule({
  declarations: [
    NotifierComponent
  ],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatButtonModule
  ],
  exports: [
    NotifierComponent
  ]
})
export class NotificationsModule { }
