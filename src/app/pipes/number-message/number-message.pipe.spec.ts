import { TestBed } from '@angular/core/testing';
import { NumberMessagePipe } from './number-message.pipe';

describe('NumberMessagePipe', () => {
  let pipe: NumberMessagePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NumberMessagePipe],
    });
    pipe = TestBed.inject(NumberMessagePipe);
  });

  it('pipe number message is created', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform NumberMessagePipe when passengersNumber is 0', () => {
    const transformedValue = pipe.transform(0);
    expect(transformedValue).toEqual('Aún no hay pasajeros registrados');
  });

  it('should transform NumberMessagePipe when passengersNumber is not 0', ()=>{
    const transformedValue = pipe.transform(4);
    expect(transformedValue).toEqual(`Pasajeros 4 de 4`);
  })

});
