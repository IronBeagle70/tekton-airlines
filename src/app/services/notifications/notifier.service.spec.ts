import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotifierComponent } from 'src/app/notifications/components/notifier/notifier.component';
import { NotifierService } from './notifier.service';

describe('NotifierService', () => {
  let notifierService: NotifierService;
  let matSnackBar: MatSnackBar;
  let matSnackBarSpy: jasmine.Spy;

  beforeEach(() => {
    matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['openFromComponent']);
    TestBed.configureTestingModule({
      providers: [
        NotifierService,
        { provide: MatSnackBar, useValue: matSnackBarSpy }
      ]
    });
    notifierService = TestBed.inject(NotifierService);
    matSnackBar = TestBed.inject(MatSnackBar);
  });

  it('notifier service should be created', ()=>{
    expect(notifierService).toBeTruthy();
  })

  it('should call openFromComponent method of MatSnackBar', () => {
    const displayMessage = 'Test message';
    const buttonText = 'Test button text';
    notifierService.showNotification(displayMessage, buttonText);
    expect(matSnackBar.openFromComponent).toHaveBeenCalledWith(
      NotifierComponent,
      jasmine.objectContaining({
        duration: 5000,
        data: { message: displayMessage, buttonText: buttonText },
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      })
    );
  });
});
