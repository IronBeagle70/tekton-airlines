import { Injectable } from '@angular/core';
import { Passenger } from '../../models/passenger.model';
import { BehaviorSubject } from 'rxjs';
import { NotifierService } from '../notifications/notifier.service';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  private passengers: Passenger[] = [];
  private passengersSubject = new BehaviorSubject<Passenger[]>(this.passengers);

  passengers$ = this.passengersSubject.asObservable();

  constructor(private notifierService: NotifierService) {}

  addPassenger(passenger: Passenger) {
    if (this.passengers.length >= 4) {
      this.notifierService.showNotification(
        'No se puede registrar más de cuatro pasajeros',
        'Ok'
      );
      return;
    }
    this.passengers.push(passenger);
    this.passengersSubject.next(this.passengers);
    this.notifierService.showNotification(
      'Pasajero Registrado Satisfactoriamente',
      'Ok'
    );
  }

  getPassengers() {
    return this.passengers;
  }

  removePassenger(index: number) {
    this.passengers = this.passengers.filter((passenger, i) => i !== index);
    this.passengersSubject.next(this.passengers);
    this.notifierService.showNotification(
      'Pasajero Eliminado Satisfactoriamente',
      'Ok'
    );
  }
}
