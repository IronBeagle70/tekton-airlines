import { TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';

import { ValidatorsService } from './validators.service';

describe('ValidatorsService', () => {
  let service: ValidatorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidatorsService],
    });
    service = TestBed.inject(ValidatorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set DNI validators correctly', () => {
    const control = new FormControl('');
    service.setupDocumentNumberValidators(control, 'DNI');

    control.setValue('');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['required']).toBeTruthy();

    control.setValue('12345');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['dniMustBeValid']).toBeTruthy();

    control.setValue('abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['dniMustBeValid']).toBeTruthy();

    control.setValue('12345abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['dniMustBeValid']).toBeTruthy();

    control.setValue('123456789');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['dniMustBeValid']).toBeTruthy();

    control.setValue('12345678');
    expect(control.valid).toBe(true);
  });

  it('should set CE validators correctly', () => {
    const control = new FormControl('');
    service.setupDocumentNumberValidators(control, 'CE');

    control.setValue('');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['required']).toBeTruthy();

    control.setValue('12345');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['ceMustBeValid']).toBeTruthy();

    control.setValue('abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['ceMustBeValid']).toBeTruthy();

    control.setValue('12345abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['ceMustBeValid']).toBeTruthy();

    control.setValue('12345678');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['ceMustBeValid']).toBeTruthy();

    control.setValue('1234567890');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['ceMustBeValid']).toBeTruthy();

    control.setValue('123456789');
    expect(control.valid).toBe(true);

    control.setValue('123abc789');
    expect(control.valid).toBe(true);
  });

  it('should set Passport validators correctly', () => {
    const control = new FormControl('');
    service.setupDocumentNumberValidators(control, 'Pasaporte');

    control.setValue('');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['required']).toBeTruthy();

    control.setValue('12345');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['passportMustBevalid']).toBeTruthy();

    control.setValue('abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['passportMustBevalid']).toBeTruthy();

    control.setValue('123456abc');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['passportMustBevalid']).toBeTruthy();

    control.setValue('1234567890');
    expect(control.invalid).toBe(true);
    expect(control.errors?.['passportMustBevalid']).toBeTruthy();

    control.setValue('123456789');
    expect(control.valid).toBe(true);
  });
});
