import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { By } from '@angular/platform-browser';
import { Passenger } from 'src/app/models/passenger.model';
import { UpdateFormComponent } from '../update-form/update-form.component';

import { PassengerComponent } from './passenger.component';

describe('PassengerComponent', () => {
  let component: PassengerComponent;
  let fixture: ComponentFixture<PassengerComponent>;
  let el: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, MatIconModule],
      declarations: [PassengerComponent, UpdateFormComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PassengerComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      });
  });

  it('passenger component should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display passenger containers', () => {
    const passengerContainer = el.nativeElement.querySelectorAll('.passenger');
    expect(passengerContainer.length).toBe(1);

    const passengerData = el.nativeElement.querySelectorAll('.passenger div');
    expect(passengerData.length).toBe(5);
  });

  it('should display passenger data', () => {
    const passenger: Passenger = {
      names: 'names test',
      surnames: 'surnames test',
      nationality: 'nationality test',
      documentType: 'DNI',
      documentNumber: '321',
    };
    component.passenger = passenger;
    fixture.detectChanges();

    const dataContainer = el.nativeElement.querySelectorAll('.passenger__data');
    expect(dataContainer.length).toBe(4);

    const span = el.nativeElement.querySelectorAll('span');
    expect(span[0].textContent).toContain(
      `${passenger.names.toUpperCase()} ${passenger.surnames.toUpperCase()} `
    );
    expect(span[1].textContent).toContain(passenger.nationality.toUpperCase());
    expect(span[2].textContent).toContain(passenger.documentType.toUpperCase());
    expect(span[3].textContent).toContain(passenger.documentNumber);
  });

  it('should display passenger icons', () => {
    const icon = el.nativeElement.querySelectorAll('mat-icon');
    expect(icon.length).toBe(4);

    expect(icon[0].textContent).toEqual('person');
    expect(icon[1].textContent).toEqual('public');
    expect(icon[2].textContent).toEqual('badge');
    expect(icon[3].textContent).toEqual('tag');
  });

  it('should display update and delete buttons', () => {
    const btnElements = el.nativeElement.querySelectorAll(
      '.btn__container button'
    );
    expect(btnElements.length).toBe(2);

    expect(btnElements[0].textContent).toEqual('Actualizar');
    expect(btnElements[1].textContent).toEqual('Eliminar');
  });

  it('should call onEditPassenger when edit button is clicked', () => {
    spyOn(component, 'onEditPassenger');

    const editButton = fixture.debugElement.queryAll(By.css('button'))[0];
    editButton.triggerEventHandler('click', null);

    expect(component.onEditPassenger).toHaveBeenCalled();
  });

  it('should call onDeletePassenger when delete button is clicked', () => {
    spyOn(component, 'onDeletePassenger');

    const deleteButton = fixture.debugElement.queryAll(By.css('button'))[1];
    deleteButton.triggerEventHandler('click', null);

    expect(component.onDeletePassenger).toHaveBeenCalled();
  });
});
