import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Passenger } from 'src/app/models/passenger.model';
import { StoreService } from 'src/app/services/store/store.service';
import { UpdateService } from 'src/app/services/updateForm/update.service';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.scss']
})
export class PassengerComponent implements OnInit{
  @Input() passenger!: Passenger;
  private subscription: Subscription = new Subscription();

  isEditing = false;

  constructor(private storeService: StoreService, private updateService: UpdateService){}

  ngOnInit(): void {
    this.subscription = this.storeService.passengers$.subscribe();
    this.updateService.isEditing$.subscribe(value => {
      this.isEditing = value;
    });
  }

  onDeletePassenger(){
    const index = this.storeService.getPassengers().indexOf(this.passenger);
    this.storeService.removePassenger(index);
    this.subscription.unsubscribe();
  }

  onEditPassenger() {
    // this.updateService.setIsEditing(true);
    this.isEditing = true;
  }

}
