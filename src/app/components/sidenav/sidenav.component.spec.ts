import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { Observable } from 'rxjs';
import { ToolbarComponent } from '../toolbar/toolbar.component';

import { SidenavComponent } from './sidenav.component';

describe('SidenavComponent', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSidenavModule,
        MatDividerModule,
        RouterModule,
        BrowserAnimationsModule,
        MatToolbarModule,
      ],
      declarations: [SidenavComponent, ToolbarComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SidenavComponent);
        component = fixture.componentInstance;
      });
  });

  it('sidenav should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should set the sidenav mode to over when the screen is small', () => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', [
      'open',
      'close',
    ]);
    sidenav.mode = 'over';
    component.sidenav = sidenav;
    const observer = TestBed.inject(BreakpointObserver);
    spyOn(observer, 'observe').and.returnValue({
      pipe: () => ({
        subscribe: (fn: (state: { matches: boolean }) => void) => {
          fn({ matches: true });
        },
      }),
    } as Observable<BreakpointState>);

    component.ngAfterViewInit();

    expect(component.sidenav.mode).toEqual('over');
    expect(component.sidenav.close).toHaveBeenCalled();
  });

  it('should set the sidenav mode to side when the screen is large', () => {
    const sidenav = jasmine.createSpyObj<MatSidenav>('MatSidenav', [
      'open',
      'close',
    ]);
    sidenav.mode = 'side';
    component.sidenav = sidenav;

    const observer = TestBed.inject(BreakpointObserver);
    spyOn(observer, 'observe').and.returnValue({
      pipe: () => ({
        subscribe: (fn: (state: { matches: boolean }) => void) => {
          fn({ matches: false });
        },
      }),
    } as Observable<BreakpointState>);

    component.ngAfterViewInit();

    expect(component.sidenav.mode).toEqual('side');
    expect(component.sidenav.open).toHaveBeenCalled();
  });
});
