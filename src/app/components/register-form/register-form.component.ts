import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Passenger } from 'src/app/models/passenger.model';
import { StoreService } from 'src/app/services/store/store.service';
import { ValidatorsService } from 'src/app/services/validators/validators.service';

import { FormControl, FormGroup, Validators } from '@angular/forms';

import { namesMustbeValid } from 'src/app/form-extensions/validators/names.validator';
import { surnamesMustbeValid } from 'src/app/form-extensions/validators/surnames.validator';
import { nationalityMustbeValid } from 'src/app/form-extensions/validators/nationality.validator';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  documentType = ['DNI', 'CE', 'Pasaporte'];

  passengerForm = new FormGroup({
    names: new FormControl('', [namesMustbeValid]),
    surnames: new FormControl('', [surnamesMustbeValid]),
    nationality: new FormControl('', [nationalityMustbeValid]),
    documentType: new FormControl('', [Validators.required]),
    documentNumber: new FormControl('', [Validators.required]),
  });

  ngOnInit() {
    this.documentTypeControl?.valueChanges.subscribe((value) => {
      this.validatorsService.setupDocumentNumberValidators(
        this.documentNumberControl,
        value
      );
    });
  }

  constructor(
    private storeService: StoreService,
    private router: Router,
    private validatorsService: ValidatorsService
  ) {}

  get namesControl(): FormControl {
    return this.passengerForm.get('names') as FormControl;
  }

  get surnamesControl(): FormControl {
    return this.passengerForm.get('surnames') as FormControl;
  }

  get nationalityControl(): FormControl {
    return this.passengerForm.get('nationality') as FormControl;
  }

  get documentTypeControl(): FormControl {
    return this.passengerForm.get('documentType') as FormControl;
  }

  get documentNumberControl(): FormControl {
    return this.passengerForm.get('documentNumber') as FormControl;
  }

  onCreatePassenger() {
    const passenger = new Passenger(
      this.namesControl.value ?? '',
      this.surnamesControl.value ?? '',
      this.nationalityControl.value ?? '',
      this.documentTypeControl.value ?? '',
      this.documentNumberControl.value ?? ''
    );
    this.storeService.addPassenger(passenger);
    this.redirectToSummary();
  }

  onCancel() {
    this.passengerForm.reset();
  }

  redirectToSummary() {
    this.passengerForm.reset();
    this.router.navigate(['/summary']);
  }
}
