import { TestBed } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        MatDividerModule,
        MatToolbarModule,
      ],
      declarations: [AppComponent, SidenavComponent, ToolbarComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);

    const app = fixture.componentInstance;
    
    expect(app).toBeTruthy();
  });

  it('sidenav component should be created', () => {
    const fixture = TestBed.createComponent(AppComponent);

    const content =
      fixture.debugElement.nativeElement.querySelectorAll('app-sidenav');
    console.log(content[0]);

    expect(content[0]).toBeTruthy();
  });
});
