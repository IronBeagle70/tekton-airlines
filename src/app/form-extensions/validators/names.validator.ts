import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export const namesMustbeValid: ValidatorFn = (
  control: AbstractControl 
): ValidationErrors | null => {

  const namesValue = control.value;

  const namesRegex = new RegExp(/^[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ\s.,']+$/);
  const namesPattern = namesRegex.test(namesValue);

  if(!namesValue){
    return { required : true};
  }

  if(!namesPattern){
    return { namesMustbeValid: true };
  }

  return null;
};

