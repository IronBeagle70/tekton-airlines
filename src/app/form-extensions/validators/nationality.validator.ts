import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export const nationalityMustbeValid: ValidatorFn = (
  control: AbstractControl 
): ValidationErrors | null => {

  const nationalityValue = control.value;

  const nationalityRegex = new RegExp(/^[\p{L}\s]+$/u);
  const nationalityPattern = nationalityRegex.test(nationalityValue);

  if(!nationalityValue){
    return { required : true};
  }

  if(!nationalityPattern){
    return { nationalityMustbeValid: true };
  }

  return null;
};

