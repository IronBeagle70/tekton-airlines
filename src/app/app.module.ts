import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatDividerModule } from "@angular/material/divider";
import { MatButtonModule } from "@angular/material/button";

import { RegisterComponent } from './pages/register/register.component';
import { SummaryComponent } from './pages/summary/summary.component';

import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

import { RegisterFormComponent } from './components/register-form/register-form.component';
import { UpdateFormComponent } from './components/update-form/update-form.component';

import { PassengersComponent } from './components/passengers/passengers.component';
import { PassengerComponent } from './components/passenger/passenger.component';

import { FormExtensionsModule } from './form-extensions/form-extesions.module';
import { NotificationsModule } from './notifications/notifications.module';

import { NumberMessagePipe } from './pipes/number-message/number-message.pipe';
import { BtnMessagePipe } from './pipes/button-message/btn-message.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    SummaryComponent,
    PassengersComponent,
    PassengerComponent,
    UpdateFormComponent,
    NumberMessagePipe,
    BtnMessagePipe,
    ToolbarComponent,
    SidenavComponent,
    RegisterFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormExtensionsModule,
    NotificationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
